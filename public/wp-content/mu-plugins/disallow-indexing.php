<?php
/**
 * Plugin Name:  Disallow Indexing
 * Plugin URI:   https://drivdigital.no
 * Description:  Disallow indexing on non-production environments
 * Version:      1.0.0
 */
if ( defined( 'APP_ENV' ) && APP_ENV !== 'production' && ! is_admin() ) {
    add_action( 'pre_option_blog_public', '__return_zero' );
}