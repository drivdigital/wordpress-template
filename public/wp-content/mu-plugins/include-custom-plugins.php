<?php
/**
 * Plugin Name: Include custom plugins
 * Plugin URI: https://bitbucket.org/drivdigital/wordpress-template
 * Description: Loads custom plugins from "public/plugins". They are project specific and not installed via composer.
 * Author: Eivin Landa
 * Author URI: https://forsvunnet.co.uk
 * Version: 1.0.0
 *
 * @package DrivDigital\WordpressTemplate
 */

$dirs = glob( "$public_path/plugins/*", GLOB_ONLYDIR );

/**
 * Remove the public path from plugin urls
 */
add_filter( 'plugins_url', function( $url ) {
	global $public_path;
	$url = str_replace( "/wp-content/plugins$public_path", '', $url );
	return $url;
} );

/**
 * Loop though custom code and require each dir as a plugin
 */
foreach ( $dirs as $dir ) {
	$file = "$dir/plugin.php";
	if ( ! file_exists( $file ) ) {
		continue;
	}
	wp_register_plugin_realpath( $file );
	require_once $file;
}

/**
 * Loco translation support for the custom directories.
 */
add_filter( 'loco_plugins_data', function( $data ) use ( $dirs ) {
	foreach ( $dirs as $dir ) {
		if ( ! is_dir( $dir ) ) {
			continue;
		}
		$name = basename( $dir );
		$data[ "$name/plugin.php" ] = [
			'Name'       => $name,
			'TextDomain' => $name,
			'basedir'    => dirname( $dir ),
		];
	}
	return $data;
});
