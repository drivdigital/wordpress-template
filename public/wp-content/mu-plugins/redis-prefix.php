<?php
/**
 * Plugin Name:  Redis prefix
 * Plugin URI:   https://drivdigital.no
 * Description:  Automatically configure reids cache key based on app name and environment
 * Version:      1.0.0
 */
if ( defined( 'APP_ENV' ) ) {
	if ( ! defined( 'APP_NAME' ) ) {
		wp_die( 'APP_NAME must be defined in .env' );
	}
	if ( ! defined( 'WP_CACHE_KEY_SALT' ) ) {
		$key = md5( APP_ENV . '_' . APP_NAME );
		$key = substr( $key, -5 );
		define( 'WP_CACHE_KEY_SALT', "{$key}_"  );
	}
}
