<?php
/**
 * This file is part of WordPress Template.
 *
 * @package wordpress-template
 */

// Set up common paths.
$public_path = __DIR__;
$root_path   = dirname( __DIR__ );

// Require the Composer autoloader.
$files = [
	$root_path . '/vendor/autoload.php',
	$root_path . '/vendor/drivdigital/wordpress-config/wp-config.php',
];

foreach ( $files as $file ) {
	if ( ! file_exists( $file ) ) {
		die( 'Please run composer install' );
	}

	require_once $file;
}

// Continue.
require_once ABSPATH . 'wp-settings.php';
