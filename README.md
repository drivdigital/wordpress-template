# WordPress Template

## Installation

```
composer create-project --repository https://satis.driv.digital drivdigital/wordpress-template project-name
```

## Linting

Install PHPCS and PHPMD

```
brew install php-code-sniffer

brew install phpmd
```

Using phpcs and phpmd, you need to clone the wordpress coding standards: `git clone https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards.git`

Read more about the coding standards here: (https://github.com/WordPress/WordPress-Coding-Standards)[https://github.com/WordPress/WordPress-Coding-Standards]

```
phpcs --config-set installed_paths /Users/mac/Sites/wpcs
```