#!/usr/bin/env php
<?php

// Get salt from the salt stream.
$salt = file_get_contents( 'https://api.wordpress.org/secret-key/1.1/salt/' );

if ( ! preg_match_all( "/\n*\s*define\(\s*\'([^\']+)\',\s*\'([^\']+)\'\s*\);/", $salt, $matches, PREG_SET_ORDER ) ) {
	echo "Error: Couldn't get salt." . PHP_EOL;
	die();
}

$env = '.env';

if ( ! file_exists( $env ) ) {
	echo "Warning: ENV file doesn't exist, trying to copy .env.example instead." . PHP_EOL;
	touch( $env );

	$env .= '.example';
}

$content = file_get_contents( $env );
$lines   = explode( PHP_EOL, $content );

foreach ( $matches as $match_pair ) {
	list( , $key, $value ) = $match_pair;

	foreach ( $lines as &$line ) {
		if ( ! preg_match( "/^$key=$/", $line ) ) {
			continue;
		}

		$line = "$key='$value'";
	}
	unset( $line );
}

file_put_contents( '.env', implode( PHP_EOL, $lines ) );
echo 'Done' . PHP_EOL;
